#------------------------------------------------------------------------------
# Description: Make NVidai experience on Sway better (still bad tho)
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-12-30
#------------------------------------------------------------------------------

export XDG_SESSION_TYPE=wayland
export EGL_PLATFORM=$XDG_SESSION_TYPE
export GBM_BACKEND=nvidia-drm
export __GLX_VENDOR_LIBRARY_NAME=nvidia
export WLR_NO_HARDWARE_CURSORS=1
export BROWSER=firefox

export XKB_DEFAULT_LAYOUT=dvorak
export XKB_DEFAULT_OPTIONS=caps:escape

exec sway --unsupported-gpu
