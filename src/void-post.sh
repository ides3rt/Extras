#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Void Linux post-installation script
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2023-07-01
#------------------------------------------------------------------------------

trap 'echo "Interrupt signal received" >&2; exit 130' INT

declare -r prg=${0##*/}

die ()
{
	echo "$prg: $2" >&2
	(( $1 )) &&
		exit $1
}

get ()
{
	local dir out tmp url
	for url in "$@"; {
		tmp=${url##*/src}

		out=${tmp##*/}
		dir=${tmp%/*}

		aria2c --allow-overwrite -d "$dir" -o "$out" "$url"
	}
}

(( EUID )) &&
	die 1 'required root access.'

(( $# )) &&
	die 1 "needn't arguments."

declare -r git=https://codeberg.org/ides3rt

xbps-install -Syu xbps void-repo-nonfree
xbps-install -Syu

xbps-install -y aria2 git neovim

sed -i '/^#C/s/#//' /etc/default/libc-locales
xbps-reconfigure -f glibc-locales
echo 'LANG=C.UTF-8' > /etc/locale.conf

sed -i '/xfs/s/[12]$/0/; /efi/s/[12]$/1/; /efi/{s#/boot/efi#/efi#;
	s/defaults/nosuid,nodev,noexec,defaults,fmask=0177,dmask=0077/};
	/var/s/defaults/nosuid,nodev,defaults/;
	/^UUID/s/defaults/noatime,lazytime/; s/defaults,//' /etc/fstab

umount /boot/efi
rmdir /boot/efi

mkdir /efi
mount /efi

for usr in /home/*; {
	usr=${usr##*/}
	usermod -aG wheel,lp,audio,video,kvm,input "$usr"
}

while read; do
	[[ $REPLY == *vendor_id* ]] &&
		break
done < /proc/cpuinfo

[[ $REPLY == *Intel* ]] &&
	xbps-install -y intel-ucode

while read; do
	[[ $REPLY == *VGA* ]] &&
		break
done <<< "$(lspci)"

case $REPLY in
*AMD*)
	gpu='mesa-ati-dri vulkan-loader xf86-video-amdgpu' ;;

*Intel*)
	gpu='mesa-intel-dri vulkan-loader intel-video-accel xf86-video-intel' ;;

*NVIDIA*)
	# Dependencies for _nvidia-libva-driver_.
	xbps-install -y gst-plugins-bad1-devel libva-devel meson meson-cmake-wrapper nv-codec-headers
	gpu=nvidia ;;
esac

echo 'LIBVA_MESSAGING_LEVEL=1' > /etc/libva.conf
xbps-install -y $gpu

get "$git"/Setup/raw/branch/main/src/etc/sudoers.d/30-security
chmod 440 /etc/sudoers.d/*

aria2c --allow-overwrite  -d /etc -o machine-id \
	https://raw.githubusercontent.com/Kicksecure/dist-base-files/master/etc/machine-id

rmdir /usr/local/sbin
ln -s bin /usr/local/sbin

mkdir -p -- /etc/security/{access,limits}.d
get "$git"/Setup/raw/branch/main/src/etc/security/{access.d/50-root,limits.d/50-coredump}.conf

sed -i '/^#Permitrootlogin/{s/#//; s/prohibit-password/no/};
	/^#Passwordauthentication/{s/#//; s/yes/no/}' /etc/ssh/sshd_config

sed -i '/^password/s/$/ rounds=1024000/' /etc/pam.d/passwd
sed -i '2aauth       optional   pam_faildelay.so     delay=5000000' \
	/etc/pam.d/system-login

ln -s /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d

read -d '' <<-EOF
compress="lz4"
squash_compress="lz4 -Xhc"
stdloglvl="0"
parallel="yes"
EOF
echo -n "$REPLY" > /etc/dracut.conf.d/myflags.conf

# Tell kernel to shutup.
kern_cmd='quiet loglevel=0 console=tty2'

# https://github.com/Kicksecure/security-misc/tree/master/etc/default/grub.d
kern_cmd+=' spectre_v2=on spec_store_bypass_disable=on tsx=off'
kern_cmd+=' tsx_async_abort=full,nosmt mds=full,nosmt l1tf=full,force'
kern_cmd+=' nosmt=force kvm.nx_huge_pages=force l1d_flush=on'
kern_cmd+=' mmio_stale_data=full,nosmt random.trust_bootloader=off'
kern_cmd+=' random.trust_cpu=off intel_iommu=on amd_iommu=on'
kern_cmd+=' iommu.passthrough=0 iommu.strict=1 slab_nomerge slub_debug=FZ'
kern_cmd+=' init_on_alloc=1 init_on_free=1 mce=0 pti=on vsyscall=none'
kern_cmd+=' page_alloc.shuffle=1 extra_latent_entropy debugfs=off'

# https://madaidans-insecurities.github.io/guides/linux-hardening.html#boot-kernel
kern_cmd+=' randomize_kstack_offset=on'

# Disable annoying OEM logo.
kern_cmd+=' bgrt_disable'

# These are meant for server usage, so we disable.
kern_cmd+=' libahci.ignore_sss=1'
kern_cmd+=' modprobe.blacklist=iTCO_wdt nowatchdog'

# Disable zswap as we already enabled zram.
kern_cmd+=' zswap.enabled=0'

sed -i "/LINUX_DEF/s/\".*\"/\"$kern_cmd\"/; /B_TIMEOUT/s/5/0/" /etc/default/grub

url=$git/Setup/raw/branch/main/src

[[ $gpu == nvidia ]] &&
	get "$url"/etc/modprobe.d/50-nvidia.conf
get "$url"/etc/sysctl.d/{60-security,80-memory}.conf

mkdir /etc/udev/rules.d
get "$url"/etc/udev/rules.d/60-ioschedulers.rules

for f in modprobe.d/30_security-misc.conf \
	sysctl.d/30_{security-misc,silent-kernel-printk}.conf
{
	aria2c --allow-overwrite -d /etc/"${f%/*}" -o "${f##*/}" \
		https://raw.githubusercontent.com/Kicksecure/security-misc/master/etc/"$f"
}
sed -i 's#/bin/.*#/bin/false#' /etc/modprobe.d/30_security-misc.conf

xbps-install -y arc-icon-theme arc-theme bash-completion bspwm rusty-diceware \
	dbus-elogind dbus-elogind-libs dbus-elogind-x11 dnscrypt-proxy dunst \
	elogind exfatprogs exiftool fcron ffmpeg firefox fzf gnupg2 htop \
	jitterentropy links maim mate-polkit mpv ncurses-term nsxiv lz4 xz \
	numlockx p7zip-unrar redshift rsync sxhkd tmux udisks2 unzip words-en \
	xsel xf86-input-evdev xorg-apps xorg-minimal yt-dlp zip zstd pcmanfm \
	gnome-keyring font-ibm-plex-otf xdg-user-dirs pipewire wireplumber

# Dependencies for _st_.
xbps-install -y gcc libX11-devel libXft-devel libXinerama-devel pkg-config

get "$git"/Setup/raw/branch/main/src/etc/cron.weekly/fstrim
chmod 755 /etc/cron.weekly/fstrim

aria2c --allow-overwrite "$git"/Extras/raw/branch/main/src/zram-setup.sh
bash -p zram-setup.sh
rm zram-setup.sh

read -d '' <<-EOF
#------------------------------------------------------------------------------
# Description: Conguration for /run/user/\$UID via elogind
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-06-25
#------------------------------------------------------------------------------

[Login]
RuntimeDirectorySize=4M
EOF

mkdir /etc/elogind/logind.conf.d
echo -n "$REPLY" > /etc/elogind/logind.conf.d/50-run-user.conf

get "$git"/Setup/raw/branch/main/src/etc/X11/Xwrapper.config

sed -i '/encryption/s/luks1/luks2/' /etc/udisks2/udisks2.conf
get "$git"/Setup/raw/branch/main/src/etc/udisks2/mount_options.conf

echo jitterentropy_rng > /etc/modules-load.d/jitterentropy.conf
ln -s /etc/sv/{dbus,elogind,fcron} /var/service

echo 'localhost.localdomain' > /etc/hostname

read -d '' <<-EOF
127.0.0.1 localhost
::1 localhost
127.0.1.1 localhost.localdomain
EOF
echo -n "$REPLY" > /etc/hosts

read -d '' <<-EOF

# Speed up DHCP by disabling ARP probing.
noarp

# Prevent the overwriting of /etc/resolv.conf
nohook resolv.conf

# Disable IPv6 Router Advertisements.
noipv6rs

# Enables Anonymity Profiles for DHCP.
randomise_hwaddr
anonymous
EOF
echo -n "$REPLY" >> /etc/dhcpcd.conf

sed -i -e "32{s/# //;
	s/\[.*\]/['quad9-dnscrypt-ip4-filter-pri', 'quad9-dnscrypt-ip6-filter-pri']/};
	/^listen/s/]/, '[::1]:53']/" -e '/^ipv6/s/false/true/;
	/^doh/s/true/false/; /_dnssec/s/false/true/; /nofil/s/true/false/;
	/^time/s/5000/10000/; /keepalive/s/30/60/; 146s/# //; 251s/,.*/]/;
	681,686s/^  /&# /; 690,695s/^  /&# /; 714,718s/# //;
	/skip_/s/false/true/; /direct_/s/# //; 717a\    refresh_delay = 72' \
	/etc/dnscrypt-proxy.toml

get "$git"/Setup/raw/branch/main/src/etc/resolv.conf
chattr +i /etc/resolv.conf

ln -s /etc/sv/dnscrypt-proxy /var/service

sed -i "s/022/077/; /sbin'$/d; /'\/bin'$/d" /etc/{login.defs,profile}
dracut -f  --regenerate-all
update-grub
