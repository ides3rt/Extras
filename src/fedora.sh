#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: An Fedora Everything post-installation script
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-12-07
#------------------------------------------------------------------------------

trap 'echo "Interrupt signal received" >&2; exit 130' SIGINT

declare -r prog=${0##/*}
declare -r git=https://codeberg.org/ides3rt

die ()
{
	echo "$prog: $2" >&2
	(( $1 )) &&
		exit $1
}

((EUID)) &&
	die 1 'must run with root privileges...'

(( $# )) &&
	die 1 "needn't arguments..."

# Unset some variables, so users can't mess it up.
unset gpu

get ()
{
	local f
	for f in "$@"; {
		curl -so "${f##*/src}" "$f"
	}
}

sed -i -E '/^(#|$)/d; s/\s+/ /g; / \/ /s/defaults/noatime/; /var/s/defaults/nosuid,nodev,noatime/;
	/efi/s/umask=0077/nosuid,nodev,noexec,noatime,fmask=0177,dmask=0077/' /etc/fstab

read -d '' <<-EOF
tmpfs /dev/shm tmpfs nosuid,nodev,noexec,size=1G 0 0
tmpfs /tmp tmpfs nosuid,nodev,size=6G 0 0
EOF
echo -n "$REPLY" >> /etc/fstab

read -d '' <<-EOF
defaultyes=True
max_parallel_downloads=20
EOF
echo -n "$REPLY" >> /etc/dnf/dnf.conf

read -d '' <<-EOF
%_install_langs en
%_netsharedpath %{_datadir}/doc:%{_datadir}/gtk-doc:%{_datadir}/info
EOF
echo -n "$REPLY" > /etc/rpm/macros

rm -r /usr/share/{{,gtk-}doc,info}
dnf -y upgrade
dnf -y reinstall glibc-common

dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

while read; do
	[[ $REPLY == *VGA* ]] &&
		break
done <<< "$(lspci)"

case $REPLY in
*AMD*)
	gpu=xorg-x11-drv-amdgpu ;;

*Intel*)
	gpu=xorg-x11-drv-intel ;;

*NVIDIA*)
	gpu='akmod-nvidia nvidia-vaapi-driver xorg-x11-drv-nvidia-cuda' ;;
esac
dnf -y install $gpu

dnf -x PackageKit-gstreamer-plugin --setopt=install_weak_deps=False -y \
	group upgrade Multimedia

echo 'LANG="C.UTF-8"' > /etc/locale.conf

echo 'localhost.localdomain' > /etc/hostname
read -d '' <<-EOF
127.0.0.1 localhost
::1 localhost
127.0.1.1 localhost.localdomain
EOF
echo -n "$REPLY" >> /etc/hosts

dnf -y install jitterentropy
echo 'jitterentropy_rng' > /usr/lib/modules-load.d/jitterentropy.conf

read -d '' <<-EOF
blacklist nouveau
options nouveau modeset=0
EOF
echo -n "$REPLY" > /etc/modprobe.d/blacklist-nouveau.conf

read -d '' <<-EOF
compress="cat"
squash_compress="lz4 -Xhc"
do_strip="no"
hostonly="yes"
tmpdir="/tmp"
stdloglvl="0"
enhanced_cpio="yes"
parallel="yes"
EOF
echo -n "$REPLY" > /etc/dracut.conf.d/myflags.conf

dnf -y install lz4
dracut -f --regenerate-all

# Tell kernel to shutup.
kern_cmd='loglevel=0 rd.udev.log_level=0 rd.systemd.show_status=false'

# https://github.com/Kicksecure/security-misc/tree/master/etc/default/grub.d
kern_cmd+=' spectre_v2=on spec_store_bypass_disable=on tsx=off'
kern_cmd+=' tsx_async_abort=full,nosmt mds=full,nosmt l1tf=full,force'
kern_cmd+=' nosmt=force kvm.nx_huge_pages=force l1d_flush=on'
kern_cmd+=' mmio_stale_data=full,nosmt random.trust_bootloader=off'
kern_cmd+=' random.trust_cpu=off intel_iommu=on amd_iommu=on'
kern_cmd+=' iommu.passthrough=0 iommu.strict=1 slab_nomerge slub_debug=FZ'
kern_cmd+=' init_on_alloc=1 init_on_free=1 pti=on vsyscall=none'
kern_cmd+=' page_alloc.shuffle=1 extra_latent_entropy debugfs=off'

# https://madaidans-insecurities.github.io/guides/linux-hardening.html#boot-kernel
kern_cmd+=' randomize_kstack_offset=on'

# Disable annoying OEM logo.
kern_cmd+=' bgrt_disable'

# These are meant for server usage, so we disable.
kern_cmd+=' libahci.ignore_sss=1'
kern_cmd+=' modprobe.blacklist=iTCO_wdt nowatchdog'

# Disable zswap as we already enabled zram.
kern_cmd+=' zswap.enabled=0'

sed -i '/TIMEOUT/s/5/0/' /etc/default/grub
grubby --args="$kern_cmd" --update-kernel=ALL
grub2-mkconfig -o /boot/grub2/grub.cfg

url=$git/Setup/raw/branch/main/src

get "$url"/etc/sysctl.d/{60-security,80-memory}.conf
get "$url"/etc/udev/rules.d/60-ioschedulers.rules

url=https://raw.githubusercontent.com/Kicksecure/security-misc/master
for f in "$url"/etc/modprobe.d/30_security-misc.conf \
	"$url"/etc/sysctl.d/30_{security-misc,silent-kernel-printk}.conf
{
	curl -so "${f##*/master}" "$f"
}
sed -i 's#/bin/.*#/bin/false#' /etc/modprobe.d/30_security-misc.conf

mkdir /etc/systemd/logind.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/logind.conf.d/50-run-user.conf

mkdir /etc/systemd/journald.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/journald.conf.d/50-max-use.conf

ln -s bash /usr/bin/rbash
curl -so /etc/machine-id \
	https://raw.githubusercontent.com/Kicksecure/dist-base-files/master/etc/machine-id
sed -i '/required/s/#auth/auth /' /etc/pam.d/su

get "$git"/Setup/raw/branch/main/src/etc/sudoers.d/30-security
chmod 440 /etc/sudoers.d/30-security

mkdir -p -- /etc/security/{access,limits}.d
get "$git"/Setup/raw/branch/main/src/etc/security/{access.d/50-root,limits.d/50-coredump}.conf

mkdir /etc/systemd/{system,user}.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/{system,user}.conf.d/50-only-native-arch.conf

mkdir /etc/systemd/coredump.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/coredump.conf.d/50-disable-coredump.conf

dnf -y install arc-theme aria2 bspwm dunst fzf git gnome-keyring htop neovim \
	maim mate-polkit mpv numlockx p7zip p7zip-plugins papirus-icon-theme \
	pcmanfm perl-Image-ExifTool redshift sxhkd sxiv tmux unrar xinput \
	xorg-x11-xinit xrandr xsetroot libX11-devel libXft-devel links \
	java-latest-openjdk qt5-qtbase qt5-qtbase-devel xset xdg-user-dirs \
	xscreensaver firefox ibm-plex-fonts-all gimp diceware

read -d '' <<-EOF
{
	"policies": {
		"NoDefaultBookmarks": true,
		"Preferences": {
			"browser.newtabpage.pinned": {
				"Value": "[]",
				"Status": "default"
			}
		}
	}
}
EOF
echo -n "$REPLY" > /usr/lib64/firefox/distribution/policies.json

dnf -y install flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
ln -s fusermount3 /usr/bin/fusermount

#flatpak install -y flathub com.github.Eloston.UngoogledChromium
#flatpak override --nofilesystem=home com.github.Eloston.UngoogledChromium

get "$git"/Setup/raw/branch/main/src/etc/X11/Xwrapper.config
ln -s /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d

sed -i '/encryption/s/luks1/luks2/' /etc/udisks2/udisks2.conf
get "$git"/Setup/raw/branch/main/src/etc/udisks2/mount_options.conf

sed -i '/^#PermitRootLogin/{s/#//; s/prohibit-password/no/};
	/^#PasswordAuthentication/{s/#//; s/yes/no/}' /etc/ssh/sshd_config
systemctl disable --now sshd

mkdir /etc/systemd/system/getty@.service.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/system/getty@.service.d/activate-numlock.conf

dnf -y install dnf-automatic
sed -i '/^up/s/default/security/; /^ap/s/no/yes/' /etc/dnf/automatic.conf
systemctl enable --now dnf-automatic.timer

mkdir /etc/systemd/resolved.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/resolved.conf.d/99-quad9.conf

nmcli connection modify enp5s0 ipv4.ignore-auto-dns yes \
	ipv6.ignore-auto-dns yes ipv6.ip6-privacy 2

sed -i '/^Def/s/public/drop/' /etc/firewalld/firewalld.conf

sed -i '/^UMASK/s/022/077/' /etc/login.defs
