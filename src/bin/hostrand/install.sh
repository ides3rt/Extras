#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Installation script for `hostrand`
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-06-05
#------------------------------------------------------------------------------
# Dependencies:
#       bash
#       coreutils
#       sed
#------------------------------------------------------------------------------

declare -r progrm=${0##*/}
declare -r dict=/usr/share/dict/american-english

die ()
{
	echo "$progrm: $2" >&2
	(( $1 )) &&
		exit $1
}

((EUID)) &&
	die 1 'must run with root privileges.'

(( $# )) &&
	die 1 "needn't arguments."

declare -i deps_err=0
for bin in sed shuf; {
	type -P "$bin" &>/dev/null || {
		die 0 "dependency, \`$bin\`, not met."
		(( deps_err++ ))
	}
}

(( deps_err )) &&
	die 1 "$deps_err dependency(s) missing, aborted."

unset -v bin deps_err

[[ -f $dict && -r $dict ]] ||
	die 1 "$dict: not found or unreadable."

src=$(realpath "$0")
declare -r src=${src%/*}

cp -v -- "$src"/hostrand /usr/local/sbin
cp -v -- "$src"/hostrand.service /etc/systemd/system

chown -c 0:0 /usr/local/sbin/hostrand
chown -c 0:0 /etc/systemd/system/hostrand.service

chmod -c 755 /usr/local/sbin/hostrand
chmod -c 644 /etc/systemd/system/hostrand.service

systemctl enable hostrand
