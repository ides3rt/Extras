#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: An Arch Linux installer
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Extras
# Last Modified: 2022-12-10
#------------------------------------------------------------------------------

trap 'echo "Interrupt signal received" >&2; exit 130' SIGINT

declare -r prog=${0##/*}

die ()
{
	echo "$prog: $2" >&2
	(( $1 )) &&
		exit $1
}

((EUID)) &&
	die 1 'must run with root privileges...'

(( $# )) &&
	die 1 "needn't arguments..."

# Unset some variables, so users can't mess it up.
unset disk p btr_trim esp_trim opt_pkg

declare -r git=https://codeberg.org/ides3rt

while read; do
	[[ $REPLY == *vendor_id* ]] &&
		break
done < /proc/cpuinfo

case $REPLY in
*AMD*)
	cpu=amd ;;

*Intel*)
	cpu=intel ;;
esac
declare -r cpu

while read; do
	[[ $REPLY == *VGA* ]] &&
		break
done <<< "$(lspci)"

case $REPLY in
*AMD*)
	gpu=xf86-video-amdgpu ;;

*Intel*)
	gpu=xf86-video-intel ;;

*NVIDIA*)
	gpu=nvidia-open-dkms ;;
esac
declare -r gpu

proc=$(( `nproc` + 1 ))

read _ kib _ < /proc/meminfo
haft_gib=$(numfmt --from-unit=K --to=iec --to-unit=G "$kib")
haft_gib=$(( ${haft_gib%G} / 2 ))

# Gentoo Wiki recommended to have 2 GiB of RAM per CPU-core.
(( proc > haft_gib )) &&
	proc=$haft_gib

curl -s "$git"/Setup/raw/branch/main/src/etc/makepkg.conf |
	sed -E "/MAKEFLAGS=/s/[0-9]+/$proc/g" > /etc/makepkg.conf

curl -s "$git"/Setup/raw/branch/main/src/etc/pacman.conf |
	sed -E "/ParallelDownloads/s/[0-9]+/$proc/g" > /etc/pacman.conf

unset -v proc kib haft_gib

get ()
{
	local f
	for f in "$@"; {
		curl -so "${f##*/src}" "$f"
	}
}

_part_disk ()
{
	# If "$disk" is empty then exit
	[[ -n $disk ]] || exit 1

	read -d '' <<-EOF
	label: gpt

	size=+512M, type=U
	size=+, type="Linux root (x86-64)"
	EOF
	echo -n "$REPLY" | sfdisk "$disk"
}

_fmt_disk ()
{
	mkfs.fat -F 32 -n ESP "$disk$p"1

	mkfs.btrfs -f -L Arch_Linux "$disk$p"2
	mount "$disk$p"2 /mnt
	btrfs su cr /mnt/@

	btrfs su cr /mnt/@/boot
	chattr +C /mnt/@/boot
	chmod 700 /mnt/@/boot

	btrfs su cr /mnt/@/home
	btrfs su cr /mnt/@/opt

	btrfs su cr /mnt/@/root
	chmod 700 /mnt/@/root

	btrfs su cr /mnt/@/srv
	btrfs su cr /mnt/@/usr_local

	btrfs su cr /mnt/@/var
	btrfs su cr /mnt/@/var_tmp
	chattr +C /mnt/@/var{,_tmp}

	mkdir /mnt/@/var/tmp
	chmod 1777 /mnt/@/var{/tmp,_tmp}

	btrfs su cr /mnt/@/.snapshots
	mkdir /mnt/@/.snapshots/0
	btrfs su cr /mnt/@/.snapshots/0/snapshot
	btrfs su set-default /mnt/@/.snapshots/0/snapshot

	umount /mnt
}

_mnt_disk ()
{
	mount -o nodev,noatime,lazytime,compress-force=zstd:1$btr_trim "$disk$p"2 /mnt

	mkdir -p /mnt/{.snapshots,boot,efi,home,opt,root,srv,usr/local,var}
	chattr +C /mnt/{boot,efi,var}
	chmod 700 /mnt/{boot,efi,root}

	mount -o nosuid,nodev,noexec,noatime,lazytime,fmask=0177,dmask=0077$esp_trim "$disk$p"1 /mnt/efi
	mount -o nosuid,nodev,noexec,noatime,subvol=@/boot "$disk$p"2 /mnt/boot
	mount -o nosuid,nodev,noatime,subvol=@/home "$disk$p"2 /mnt/home
	mount -o nodev,noatime,subvol=@/opt "$disk$p"2 /mnt/opt
	mount -o nosuid,nodev,noatime,subvol=@/root "$disk$p"2 /mnt/root
	mount -o nosuid,nodev,noexec,noatime,subvol=@/srv "$disk$p"2 /mnt/srv
	mount -o nodev,noatime,subvol=@/usr_local "$disk$p"2 /mnt/usr/local
	mount -o nosuid,nodev,noexec,noatime,subvol=@/var "$disk$p"2 /mnt/var
	mount -o nosuid,nodev,noatime,subvol=@/var_tmp "$disk$p"2 /mnt/var/tmp
	mount -o nodev,noatime,subvol=@/.snapshots "$disk$p"2 /mnt/.snapshots

	mkdir -p /mnt/state/var
	chattr +C /mnt/state/var

	mkdir -p /mnt/{,state/}var/lib/pacman
	mount -Bo nosuid,nodev,noexec /mnt/state/var/lib/pacman /mnt/var/lib/pacman
}

init_disk ()
{
	[[ $disk == /dev/nvme* ]] &&
		declare -r p=p

	if (( ! `< /sys/block/"${disk#/dev/}"/queue/rotational` )); then
		declare -r btr_trim=',discard=async'
		declare -r esp_trim=',discard'
	fi

	_part_disk ||
		exit 1

	_fmt_disk
	_mnt_disk
}

if (( `stat -c %i /` == `stat -c %i /proc/1/root/.` )); then
	loadkeys dvorak

	PS3='Select your disk: '
	select disk in $(lsblk -dne 7 -o Path); do
		if init_disk; then
			break
		else
			die 1 'failed to partition disk.'
		fi
	done
	unset -v disk

	# Create dummies, so systemd(1) doesn't make random subvol.
	mkdir -p /mnt/var/lib/{machines,portables}
	chmod 700 /mnt/var/lib/{machines,portables}

	# Allow /usr/bin/sh installation for once,
	# else A LOT of pacman's hooks won't work.
	sed -i 's,usr/bin/sh,,' /etc/pacman.conf

	pacstrap /mnt base linux-hardened linux-hardened-headers \
		linux-firmware "$cpu"-ucode neovim

	chattr +C /mnt/{dev,run,tmp,sys,proc}

	# genfstab(8) doesn't handle bind-mount properly,
	# so we need to handle [bind-mount] ourself.
	umount /mnt/var/lib/pacman

	genfstab -U /mnt | sed -E '/^#/d; s/\s+/ /g; s/rw,//;
		s/,ssd//; s/,subvolid=[0-9]+//; 5,${s/,compress.*v2//;
		s/,lazytime//}; s#/@#@#; s#,subvol=@/\.snapshots/0/snapshot##;
		/\/efi/{s/.$/1/; s/,code.*-ro//}; s/,sp.*v2//;
		/btrfs/s/0 [1-2]$/0 0/' > /mnt/etc/fstab

	mount -Bo nosuid,nodev,noexec /mnt/state/var{,}
	mount -Bo nosuid,nodev,noexec /mnt/{state/,}var/lib/pacman

	read -d '' <<-EOF
	/state/var /state/var none bind,nosuid,nodev,noexec 0 0

	/state/var/lib/pacman /var/lib/pacman none bind,nosuid,nodev,noexec 0 0

	tmpfs /dev/shm tmpfs nosuid,nodev,noexec,size=1G 0 0

	tmpfs /tmp tmpfs nosuid,nodev,size=6G 0 0

	EOF
	echo -n "$REPLY" >> /mnt/etc/fstab

	mkdir /mnt/arch-install
	mount -t tmpfs -o nosuid,nodev,noatime tmpfs /mnt/arch-install

	if [[ -f $0 ]]; then
		cp "$0" /mnt/arch-install
		bash=${0##*/}
	else
		url=$git/Extras/raw/branch/main/src/arch-install.sh
		bash=${url##*/}

		curl -so /mnt/arch-install/"$bash" "$url"
		unset -v url
	fi

	arch-chroot /mnt bash -p /arch-install/"$bash"
	unset -v bash

	umount /mnt/arch-install
	rmdir /mnt/arch-install

	# Remove /etc/resolv.conf as it's required for some
	# programs to work correctly with systemd-resolved(8).
	rm -f /mnt/etc/resolv.conf

	umount -R /mnt
	exit 0
fi

while :; do
	read -p 'Your timezone: ' zone
	[[ -f /usr/share/zoneinfo/$zone ]] &&
		break
	die 0 "$zone: not found..."
done

ln -sf /usr/share/zoneinfo/"$zone" /etc/localtime
hwclock --systohc

read -d '' <<-EOF
C.UTF-8 UTF-8
en_US.UTF-8 UTF-8
EOF
echo -n "$REPLY" > /etc/locale.gen

# The reason we also generated "en_US.UTF-8", eventhough I/we don't use it,
# because of compatibility. So, it's optional to generate it.
locale-gen
echo 'LANG=C.UTF-8' > /etc/locale.conf

read -d '' <<-EOF

127.0.0.1 localhost
::1 localhost
127.0.1.1 localhost.localdomain
EOF

echo -n "$REPLY" >> /etc/hosts
echo 'localhost.localdomain' > /etc/hostname

systemctl enable systemd-{network,resolve}d
get "$git"/Setup/raw/branch/main/src/etc/systemd/{network/20-dhcp.network,resolved.conf.d/99-quad9.conf}

disk=$(findmnt -nvo Source /)
disk=${disk%%[0-9]}
mod='sd_mod ahci'

if [[ $disk == /dev/nvme* ]]; then
	mod='nvme nvme_core'
	disk=${disk%p*}
	p=p
fi

read -d '' <<-EOF
# mkinitcpio preset file for the 'linux-hardened' package.

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-linux-hardened"
ALL_microcode=(/boot/*-ucode.img)

PRESETS=('default')

default_image="/boot/initramfs-linux-hardened.img"
default_uki="/efi/EFI/Linux/arch-linux-hardened.efi"

fallback_image="/boot/initramfs-linux-hardened-fallback.img"
fallback_uki="/efi/EFI/Linux/arch-linux-hardened-fallback.efi"
fallback_options="-S autodetect"
EOF
echo -n "$REPLY" > /etc/mkinitcpio.d/linux-hardened.preset

read -d '' <<-EOF
MODULES=($mod btrfs)
BINARIES=()
FILES=()
HOOKS=(systemd autodetect modconf)
COMPRESSION="lz4"
COMPRESSION_OPTIONS=(-12 --favor-decSpeed)
EOF
echo -n "$REPLY" > /etc/mkinitcpio.conf

mkdir -p /efi/EFI/Linux
rm -f /boot/initramfs-linux-hardened-fallback.img

mkdir /etc/pacman.d/hooks

url=$git/Setup/raw/branch/main/src/etc/pacman.d/hooks
for f in "$url"/{50-path,99-{permissions,journalctl}}.hook; {
	curl -so "${f##*/src}" "$f"
}
unset -v url f

pacman -S --noc btrfs-progs dosfstools efibootmgr sudo fakeroot \
	man-db man-pages dash dbus-broker jitterentropy words

# Pipe yes(1) into pacman(8) is required,
# else 'iptables-nft' won't be install.
yes | pacman -S --asd iptables-nft

kern_cmd="root=PARTUUID=$(findmnt -nvo PartUUID /) ro"

# We already use Unified Kernel Image, which
# required ALL 'initrd' command to be GONE.
#kern_cmd+=" initrd=\\$cpu-ucode.img initrd=\\initramfs-linux-hardened.img"

# Tell kernel to shutup.
kern_cmd+=' quiet loglevel=0 rd.udev.log_level=0 rd.systemd.show_status=false'

# https://github.com/Kicksecure/security-misc/tree/master/etc/default/grub.d
kern_cmd+=' spectre_v2=on spec_store_bypass_disable=on tsx=off'
kern_cmd+=' tsx_async_abort=full,nosmt mds=full,nosmt l1tf=full,force'
kern_cmd+=' nosmt=force kvm.nx_huge_pages=force l1d_flush=on'
kern_cmd+=' mmio_stale_data=full,nosmt random.trust_bootloader=off'
kern_cmd+=' random.trust_cpu=off intel_iommu=on amd_iommu=on'
kern_cmd+=' iommu.passthrough=0 iommu.strict=1 slab_nomerge slub_debug=FZ'
kern_cmd+=' init_on_alloc=1 init_on_free=1 pti=on vsyscall=none'
kern_cmd+=' page_alloc.shuffle=1 extra_latent_entropy debugfs=off'

# https://madaidans-insecurities.github.io/guides/linux-hardening.html#boot-kernel
kern_cmd+=' randomize_kstack_offset=on'

# Disable annoying OEM logo.
kern_cmd+=' bgrt_disable'

# These are meant for server usage, so we disable.
kern_cmd+=' libahci.ignore_sss=1'
kern_cmd+=' modprobe.blacklist=iTCO_wdt nowatchdog'

# Disable zswap as we already enabled zram.
kern_cmd+=' zswap.enabled=0'

echo "$kern_cmd" > /etc/kernel/cmdline
efibootmgr -c -d "$disk" -p 1 -L 'Arch Linux' -l /EFI/Linux/arch-linux-hardened.efi

unset -v disk p mod kern_cmd

url=$git/Setup/raw/branch/main/src

[[ $gpu == *nvidia* ]] &&
	get "$url"/etc/modprobe.d/50-nvidia.conf

get "$url"/etc/sysctl.d/{60-{security,user_ns},80-memory}.conf

get "$url"/etc/udev/rules.d/60-ioschedulers.rules

url=https://raw.githubusercontent.com/Kicksecure/security-misc/master
for f in "$url"/etc/modprobe.d/30_security-misc.conf \
	"$url"/etc/sysctl.d/30_{security-misc,silent-kernel-printk}.conf
{
	curl -so "${f##*/master}" "$f"
}
unset -v url f

sed -i 's#/bin/.*#/bin/false#' /etc/modprobe.d/30_security-misc.conf

while :; do
	read -p "Do you want to install pkg for author's dotfiles? [y/N]: "
	case ${REPLY,,} in
	yes|y)
		opt_pkg=true
		break ;;

	no|n|'')
		break ;;

	*)
		die 0 "$REPLY: invaild reply." ;;
	esac
done

if [[ $opt_pkg == true ]]; then
	# Pre-install dependencies, so it doesn't prompt
	# user what to be chosen.
	pacman -S --noc --asd jack2 ttf-ibm-plex bash-completion yt-dlp aria2 \
		xclip arc-icon-theme libnotify

	pacman -S --noc $gpu git zip unzip p7zip udisks2 exfatprogs xfsprogs \
		xorg-server xorg-xrandr xorg-xinit htop redshift bspwm sxhkd \
		xorg-xsetroot fzf sxiv maim mpv perl-image-exiftool firefox \
		arc-gtk-theme openssh numlockx alsa-utils dunst tmux pkgconfig \
		mate-polkit fuse2 fuse3 xorg-xinput gimp gtk-engine-murrine \
		gnome-themes-standard gnome-keyring jre-openjdk libreoffice-fresh

	if [[ $gpu == *nvidia* ]]; then
		pacman -S --noc --asd nvidia-settings

		systemctl enable nvidia-suspend
		systemctl mask nvidia-hibernate
	fi

	ln -s run/media /
	get "$git"/Setup/raw/branch/main/src/etc/X11/Xwrapper.config

	ln -s /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf \
		/etc/fonts/conf.d

	sed -i '/encryption/s/luks1/luks2/' /etc/udisks2/udisks2.conf
	get "$git"/Setup/raw/branch/main/src/etc/udisks2/mount_options.conf

	sed -i '/^#permitrootlogin/{s/#//; s/prohibit-password/no/};
		/^#passwordauthentication/{s/#//; s/yes/no/}' /etc/ssh/sshd_config

	mkdir /etc/systemd/system/getty@.service.d
	get "$git"/Setup/raw/branch/main/src/etc/systemd/system/getty@.service.d/activate-numlock.conf

	sed -i -e '5aauth       optional     pam_gnome_keyring.so' \
		-e '$asession    optional     pam_gnome_keyring.so auto_start' \
		/etc/pam.d/login
fi
unset -v opt_pkg dir

url=$git/Extras/raw/branch/main/src/zram-setup.sh
bash=/tmp/${url##*/}

curl -so "$bash" "$url"
bash -p "$bash"
unset -v url bash

srv_dir=/etc/systemd/system

# Force sulogin(8) to login as root.
# This is required as we disable root account.
for d in "$srv_dir"/{emergency,rescue}.service.d; {
	mkdir "$d"
	get "$git/Setup/raw/branch/main/src$d/sulogin.conf"
	sed -i "/FILES/{s#)# $d/sulogin.conf)#; s/( /(/}" /etc/mkinitcpio.conf
}
unset -v d

mkdir /etc/systemd/logind.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/logind.conf.d/50-run-user.conf

mkdir /etc/systemd/journald.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/journald.conf.d/50-max-use.conf

systemctl mask hibernate.target
systemctl disable dbus
systemctl enable dbus-broker
systemctl --global enable dbus-broker

rmdir /usr/local/sbin
ln -s bin /usr/local/sbin

ln -s bash /usr/bin/rbash
ln -sf dash /usr/bin/sh

url=https://raw.githubusercontent.com/Kicksecure/dist-base-files/master
curl -so /etc/machine-id "$url"/etc/machine-id
unset -v url

# Only allow Su if user in "wheel" group.
sed -i '/required/s/#auth/auth /' /etc/pam.d/su{,-l}
echo 'jitterentropy_rng' > /usr/lib/modules-load.d/jitterentropy.conf
sed -i '/^password/s/$/ rounds=1024000/' /etc/pam.d/passwd

mkdir -p -- /etc/security/{access,limits}.d
get "$git"/Setup/raw/branch/main/src/etc/security/{access.d/50-root,limits.d/50-coredump}.conf

mkdir /etc/systemd/{system,user}.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/{system,user}.conf.d/50-only-native-arch.conf

mkdir /etc/systemd/coredump.conf.d
get "$git"/Setup/raw/branch/main/src/etc/systemd/coredump.conf.d/50-disable-coredump.conf

get "$git"/Setup/raw/branch/main/src/etc/sudoers

while :; do
	passwd &&
		break
done

while :; do
	read -p 'Your username: ' usr
	useradd -mG wheel,plugdev "$usr" && break
done

while :; do
	passwd "$usr" &&
		break
done

echo 'KEYMAP=dvorak' > /etc/vconsole.conf

mkinitcpio -P
sed -i 's/022/077/; /\/sbin/d' /etc/profile
sed -i '/DELAY/s/3/5/; s#/usr/local/sbin:##' /etc/login.defs
